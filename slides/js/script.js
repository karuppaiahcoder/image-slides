'use strict';
//Hold current slide page index
let slideIndex = 0,waitTimeId;
//Select all slide images
const mySlides = document.querySelectorAll(".fade");
//Select previuse buton
const leftbtn = document.querySelector(".prev");
//Select next button
const rightbtn = document.querySelector(".next");
//Select indicatorss
const dots = document.querySelectorAll(".dot");

const showSlides = function () {
    let i;
    console.log(mySlides, mySlides.length);
    for (i = 0; i < mySlides.length; i++) {
        mySlides[i].classList.add("hide");
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].classList.remove("active");
    }
    slideIndex++;
    if (slideIndex > mySlides.length) slideIndex = 1;
    mySlides[slideIndex - 1].classList.remove("hide");
    dots[slideIndex - 1].classList.add("active");
    waitTimeId=setTimeout(showSlides, 5000);
};

leftbtn.addEventListener('click', function () {
    mySlides[slideIndex - 1].classList.add("hide");
    dots[slideIndex - 1].classList.remove("active");
    if (slideIndex === 1)
        slideIndex = mySlides.length;
    else
        slideIndex--;
    mySlides[slideIndex - 1].classList.remove("hide");
    dots[slideIndex - 1].classList.add("active");
    clearTimeout(waitTimeId);
    waitTimeId=setTimeout(showSlides, 5000);
});

rightbtn.addEventListener('click', function () {
    mySlides[slideIndex - 1].classList.add("hide");
    dots[slideIndex - 1].classList.remove("active");
    if (slideIndex === mySlides.length)
        slideIndex = 1;
    else
        slideIndex++;
    mySlides[slideIndex - 1].classList.remove("hide");
    dots[slideIndex - 1].classList.add("active");
    clearTimeout(waitTimeId);
    waitTimeId=setTimeout(showSlides, 5000);
});
showSlides();
